import {Component, OnInit} from '@angular/core';
import * as moment from 'moment';
import {DateService} from '../shared/date.service';
import * as fetch from 'node-fetch';
import {switchMap} from 'rxjs/operators';
import {Task, TasksService} from '../shared/tasks.service';

interface Day {
  value: moment.Moment
  active: boolean
  disabled: boolean
  selected: boolean
}

interface Week {
  days: Day[]
}

class DataService {
  url: string;

  constructor(url) {
    this.url = url;
  }

  async getUser(id) {
    try {
      let response = await fetch(`${this.url}/users/${id}`);
      let data = await response.json();

      return data;
    } catch (error) {
      throw new Error('Ooops !!! Не удалось получить данные пользователя');
    }
  }

  async getPosts(userId) {
    try {
      let response = await fetch(`${this.url}/posts?userId=${userId}`);
      let data = await response.json();

      return data;
    } catch (error) {
      throw new Error('Ooops !!! Не удалось получить сообщений');
    }
  }

  async getComments(postId) {
    try {
      let response = await fetch(`${this.url}/comments?postId=${postId}`);
      let data = await response.json();

      return data;
    } catch (error) {
      throw new Error('Ooops !!! Не удалось получить комментарии');
    }
  }
}


@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {

  calendar: Week[];

  constructor(private dateService: DateService,
              private tasksService: TasksService) {
  }

  ngOnInit() {
    // this.m.set('41', 'bla bla bla')
    // .set('42', 'bla bla bla')
    // .set('43', 'bla bla bla')
    // .set('44', 'bla bla bla')
    // console.log(key, value);
    // this.main();
    this.dateService.date.subscribe(this.generate.bind(this));
  }


  // async main() {
  //   let dataService = new DataService('https://jsonplaceholder.typicode.com');
  //   try {
  //     let user = await dataService.getUser(5);
  //     let posts = await dataService.getPosts(user.id);
  //     let comments = await dataService.getComments(posts[0].id);
  //     let k = [ 1, "Mno"];
  //     console.log([...k, comments[0]]);
  //   } catch (error) {
  //     console.error(error);
  //   }
  // }

  generate(now: moment.Moment) {
    const startDay = now.clone().startOf('month').startOf('week');
    const endDay = now.clone().endOf('month').endOf('week');

    const date = startDay.clone().subtract(1, 'day');

    const calendar = [];

    while (date.isBefore(endDay, 'day')) {
      calendar.push({
        days: Array(7)
          .fill(0)
          .map(() => {
            const value = date.add(1, 'day').clone();
            const active = moment().isSame(value, 'date');
            const disabled = !now.isSame(value, 'month');
            const selected = now.isSame(value, 'date');

            return {
              value, active, disabled, selected
            };
          })
      });
    }

    this.calendar = calendar;
  }

  select(day: moment.Moment) {
    this.dateService.changeDate(day);
  }
}
